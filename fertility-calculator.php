<?php 

  include('includes/header.php'); 
  include('includes/classes/Calculator.php');

  // should we show the results window?
  $showResults = isset($_POST['fertilityDates']) ? true : false;
  
  if($showResults) { // show results window and calculate results
    $class = '';
    $calc = new Calculator();
     $outData = $calc->calculateLmpToFertilityDates($_POST['fertilityDates']);
    
  } else {
    $class="hidden";
  }
  
  // process form if it has been submitted.
?>


    <div class="container-fluid" style="margin-top:2em;">
    
      <div class="row">
      
        <div class="col-md-4">
          <div class="well">
            
            <form action="" method="POST">
            
              <div class="form-group">
                <label>LMP to Fertility Dates</label>
                <input type="date" name="fertilityDates" class="form-control">
              </div>
              
              <div class="form-group">
                <input type="submit" class="btn btn-primary btn-block" name="submit" value="Find my Fertile Days" class="form-control">
              </div>
              
            </form>
            
          </div>
        </div>
        
        <div class="col-md-5 <?php echo $class; ?>">
        <h2>LMP: <?php echo $outData['lmpPretty']; ?></h2>
            <table class="table">
            	<thead>
            		<tr>
	            		<th>Next 6 Fertile Periods</th>
	            		<th>Due Date</th>
            		</tr>
            	</thead>
            	<tbody>
            		<?php 
            			foreach($outData['fertileDates'] as $fd) {
            		?>
            		<tr>
            			<td><?php echo $fd['start']; ?> - <?php echo $fd['end']; ?></td>
            			<td><?php echo $fd['due']; ?></td>
            		</tr>
            		<?php
            			}
            		?>
            	</tbody>
            </table>
        </div>
        
        
      </div>

    </div>

<?php include('includes/footer.php'); ?>

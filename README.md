# Pregnancy Calculator #

A tool to perform several calculations related to conception and fertility. Includes calculations for due date based on LMP or Conception date, likely Conception Date based on LMP, and next 6 fertile windows based on LMP. 
<?php 

  include('includes/header.php'); 
  include('includes/classes/PregnancyCalculator.php');
  
  // should we show the results window?
  $showResults = isset($_POST['lmpDate']) ? true : false;
  
  // selected cycleLength
  $selectedCycle = 28;
  // selected luteal phase
  $selectedLuteal = 14;
  
  if($showResults) { // show results window and calculate results
    $class = '';
    
    foreach($_POST as $k=>$v) {
      $$k = $v;
    }
    
    // update selected
    $selectedCycle = $cycleLength;
    $selectedLuteal = $lutealPhase;
    
    $calc = new PregnancyCalculator($lmpDate,$ovuDate,$cycleLength,$lutealPhase);
    $outData = $calc->calculate();
    
  } else {
    $class="hidden";
  }
  
  // process form if it has been submitted.
?>


    <div class="container-fluid" style="margin-top:2em;">
    
      <div class="row">
      
        <div class="col-md-4">
          <div class="well">
            <h3>Full Calculator</h3>
            <form action="" method="POST">
            
              <div class="form-group">
                <label class="radio-inline">
                  <input type="radio" name="calculateBy" class="calculateBy" value="1" checked> LMP Date
                </label>
                <label class="radio-inline">
                  <input type="radio" name="calculateBy" class="calculateBy" value="2"> Conception Date
                </label>
              </div>
            
              <div class="form-group" id="lmpDate">
                <label>LMP Date <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="The first day of your Last Menstrual Period"></i></label>
                <input type="date" name="lmpDate" class="form-control">
              </div>
              
              <div class="form-group hidden" id="conceptionDate">
                <label>Conception Date <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="The date that conception likely occurred."></i></label>
                <input type="date" name="ovuDate" class="form-control">
              </div>
              
              <div class="form-group">
                <label>Cycle Length <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Counted from the first day of one period to the first day of the next."></i></label>
                <select name="cycleLength" class="form-control">
                  <?php
                    for($i = 20; $i < 46; $i++) {
                  ?>
                    <option value="<?php echo $i; ?>" <?php if($i==$selectedCycle) {?> selected <?php } ?>><?php echo $i; ?></option>
                  <?php
                    }
                  ?>
                </select>
              </div>
              
              <div class="form-group">
                <label>Luteal Phase Length <i class="fa fa-question-circle" data-toggle="tooltip" data-placement="right" title="Also referred as as days post ovulation (DPO), luteal length is the number of days between ovulation up to the day before the start of your next period."></i></label>
                <select name="lutealPhase" class="form-control">
                  <?php 
                    for($i=10;$i<24;$i++) {
                  ?>
                    <option value="<?php echo $i; ?>" <?php if($i==$selectedLuteal) {?> selected <?php } ?>><?php echo $i; ?></option>
                  <?php
                    }
                  ?>
                </select>
              </div>
              
              <div class="form-group">
                <input type="submit" class="btn btn-primary btn-block" name="submit" value="Generate Report" class="form-control">
              </div>
              
            </form>
          </div>
        </div>
        
        <div class="col-md-8 <?php echo $class; ?>">
          <div class="panel panel-default">
            <div class="panel-heading">
              <h3 class="panel-title">Your Personalized Results</h3>
            </div>
            <div class="panel-body">
              
              <?php
                if($outData['showLmp']) {
                  
              ?>
                <p>Based on your LMP of <b><?php echo $outData['lmpDate']; ?></b>, your estimated conception date is <b><?php echo $outData['ovulationDate']; ?></b> and your due date is approximately <b><?php echo $outData['dueDate']; ?>!</b></p>
              <?php
                } else {
              ?>
                <p>Based on your conception date of <b><?php echo $outData['ovulationDate']; ?></b>, your estimated due date is <b><?php echo $outData['dueDate']; ?></b></p>
              <?php
                }
              ?>
              
                <table class="table">
                  <thead>
                    <tr>
                      <th>Next 6 Fertile Periods</th>
                      <th>Due Date</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php 
                      foreach($outData['fertilePeriods'] as $fd) {
                    ?>
                    <tr>
                      <td><?php echo $fd['start']; ?> - <?php echo $fd['end']; ?></td>
                      <td><?php echo $fd['due']; ?></td>
                    </tr>
                    <?php
                      }
                    ?>
                  </tbody>
                </table>
              
            </div>
          </div>
            
        </div>
        
        
      </div>

    </div>

<?php include('includes/footer.php'); ?>
<?php 

  include('includes/header.php'); 
  include('includes/classes/Calculator.php');

  // should we show the results window?
  $showResults = isset($_POST['lmpDate']) ? true : false;
  
  if($showResults) { // show results window and calculate results
    $class = '';
    $calc = new Calculator();
     $outData = $calc->calculateLmpToDueDate($_POST['lmpDate']);
    
  } else {
    $class="hidden";
  }
  
  // process form if it has been submitted.
?>


    <div class="container-fluid" style="margin-top:2em;">
    
      <div class="row">
      
        <div class="col-md-4">
          <div class="well">
            
            <form action="" method="POST">
            
              <div class="form-group">
                <label>LMP to Due Date</label>
                <input type="date" name="lmpDate" class="form-control">
              </div>
              
              <div class="form-group">
                <input type="submit" class="btn btn-primary btn-block" name="submit" value="Find my due date." class="form-control">
              </div>
              
            </form>
          </div>
        </div>
        
        <div class="col-md-5 text-right <?php echo $class; ?>">
          <h2>Your personalized report</h2>
          
            <h3><small>This is the LMP that was provided.</small> <?php echo $outData['lmpPretty']; ?></h3>
            
            <h3><small>Calculated conception date. This is 14 days after LMP.</small> <?php echo $outData['conceptionPretty']; ?>
            </h3>
            
            <h3><small>Calculated Due Date. 280 days after the LMP.</small> <?php echo $outData['dueDatePretty']; ?>
            </h3>
            
        </div>
        
        
      </div>

    </div>

<?php include('includes/footer.php'); ?>

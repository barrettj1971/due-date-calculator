<?php

// main class that holds all global methods

class Calculator {
	
	/*
	* calculate estimated Due Date from LMP
	*
	*/
	public function calculateLmpToDueDate($lmpDate) {
		$out = array();
		
		$out['lmpStamp'] = $this->dateToTime($lmpDate);
		$out['lmpPretty'] = $this->prettifyDate($out['lmpStamp']);
		
		$out['conceptionStamp'] = $this->conceptionFromLMP($out['lmpStamp']);
		$out['conceptionPretty'] = $this->prettifyDate($out['conceptionStamp']);
		
		$out['dueDateStamp'] = $this->dateFromLMP($out['lmpStamp']);
		$out['dueDatePretty'] = $this->prettifyDate($out['dueDateStamp']);
		
		return $out;
	}
	
	/*
	* calculate estimated Due Date from Conception
	*
	*/
	public function calculateConceptionToDueDate($conDate) {
		$out = array();
		
		$out['conceptionStamp'] = $this->dateToTime($conDate);
		$out['conceptionPretty'] = $this->prettifyDate($out['conceptionStamp']);
		
		$out['lmpStamp'] = $this->lmpFromConception($out['conceptionStamp']);
		$out['lmpPretty'] = $this->prettifyDate($out['lmpStamp']);
		
		$out['dueDateStamp'] = $this->dateFromLMP($out['lmpStamp']);
		$out['dueDatePretty'] = $this->prettifyDate($out['dueDateStamp']);
		
		return $out;
	}
	
	/*
	* calculate estimated Fertile periods
	*
	*/
	public function calculateLmpToFertilityDates($lmpDate,$cycle=28) {
		$out = array();
		
		$out['lmpStamp'] = $this->dateToTime($lmpDate);
		$out['lmpPretty'] = $this->prettifyDate($out['lmpStamp']);
		
		$out['fertileDates'] = $this->getFertilePeriods($out['lmpStamp']);
		
		$out['dueDateStamp'] = $this->dateFromLMP($out['lmpStamp']);
		$out['dueDatePretty'] = $this->prettifyDate($out['dueDateStamp']);
		
		return $out;
	}
	
	/*
	*	get a group of upcoming fertile periods
	*/
	public function getFertilePeriods($stamp,$cycle=28,$count=6) {
		$ret = array();
		for($i=0;$i<$count;$i++) {
			$ret[$i]['start'] = $this->prettifyDate($this->getFertileStartStamp($stamp));
			$ret[$i]['end'] = $this->prettifyDate($this->getFertileEndStamp($stamp));
			$ret[$i]['due'] = $this->prettifyDate($this->dateFromLMP($stamp));
			$stamp = strtotime('+' . $cycle . ' days',$stamp);
		}
		return $ret;
	}
	
	/*
	* calculate start of fertile period from LMP
	*/
	public function getFertileStartStamp($stamp, $cycle=28) {
		$factor = $cycle-17;
		return strtotime('+' . $factor . ' days', $stamp);
	}
	
	/*
	* calculate end of fertile period from LMP
	*/
	public function getFertileEndStamp($stamp, $cycle=28) {
		$factor = $cycle-12;
		return strtotime('+' . $factor . ' days', $stamp);
	}
	
	// take timestamp for LMP and calculate due date
	// returns timestamp
	public function dateFromLMP($stamp) {
		return strtotime('+280 days', $stamp);
	}
	
	// take timestamp for LMP and calculate likely conception date
	// returns timestamp
	public function conceptionFromLMP($stamp) {
		return strtotime('+14 days', $stamp);
	}
	
	// calculate LMP from conception date
	public function lmpFromConception($stamp) {
		return strtotime('-14 days', $stamp);
	}
	
	/**
	* convert string to timestamp
	* @ string 
	* @return timestamp
	**/
	public function dateToTime($date) {
		return strtotime($date);
	}
	
	/**
	* convert timestamp to human readable date
	* @timestamp
	* @return string
	*/
	public function prettifyDate($stamp) {
		return date('M j, Y',$stamp);
	}
	
}
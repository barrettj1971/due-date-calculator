<?php

class PregnancyCalculator {
	
	private $lmp;
	private $ovulation;
	private $cycle;
	private $luteal;
	private $showLmp = true;
	
	
	public function __construct($lmp,$ovu,$cycle,$luteal) {
		$this->init($lmp,$ovu,$cycle,$luteal);
	}
	
	public function init($lmp,$ovu,$cycle,$luteal) {
		
		$this->lmp = strtotime($lmp);
		$this->cycle = $cycle;
		$this->luteal = $luteal;
		
		// if ovulation isn't given we need to calculate it
		if(strlen($ovu)<1) {
			$this->ovulation = strtotime('+' . $this->cycle - $this->luteal . ' days', $this->lmp);
		} else {
			$this->ovulation = strtotime($ovu);
			$this->showLmp = false;
		}
	}
	
	public function calculate() {
		$out = array();
		
		$out['lmpDate'] = $this->getLmpDate();
		
		$out['dueDate'] = $this->getDueDate();
		
		$out['ovulationDate'] = $this->getOvulationDate();
		
		$out['fertilePeriods'] = $this->getFertilePeriods();
		
		$out['showLmp'] = $this->showLmp;
		
		return $out;
	}
	
	/*
	*	calculate lmp date
	*/
	private function getLmpDate() {
		return $this->prettifyDate($this->lmp);
	}
	
	/*
	* calculate due date
	*/
	private function getDueDate() {
		// do we have ovulation date? 
		if(strlen($this->ovulation)>0) {
			return $this->ovulationToDueDate();
		} else {
			return $this->lmpToDueDate();
		}
	}
	
	/*
	*	calculate ovulation date
	*/
	private function getOvulationDate() {
		return $this->prettifyDate($this->ovulation);
	}
	
	/*
	*	calculate fertility periods for next 6 cycles
	*/
	private function getFertilePeriods() {
		//$stamp = strtotime('+' . $this->cycle . ' days', $this->ovulation);
		$stamp = $this->ovulation;
		$ret = array();
		
		for($i=0;$i<6;$i++) {
			$ret[$i]['start'] = $this->prettifyDate($this->getFertileStartStamp($stamp));
			$ret[$i]['end'] = $this->prettifyDate($this->getFertileEndStamp($stamp));
			$ret[$i]['due'] = $this->ovulationToDueDate($stamp);
			$stamp = strtotime('+' . $this->cycle . ' days',$stamp);
		}
		
		return $ret;
	}
	
	/*
	*	due date from ovulation/conception
	*/
	private function ovulationToDueDate($stamp = false) {
		if(!$stamp) {
			$stamp = $this->ovulation;
		}
		$stamp = strtotime('+266 days',$stamp);
		return $this->prettifyDate($stamp);
	}
	
	/*
	*	due date from LMP
	*/
	private function lmpToDueDate($stamp = false) {
		if(!$stamp) {
			$stamp = $this->lmp;
		}
		
		$ret = strtotime('+280 days',$stamp);
		return $this->prettifyDate($ret);
	}
	
	/**
	* convert timestamp to human readable date
	* @timestamp
	* @return string
	*/
	public function prettifyDate($stamp) {
		return date('M j, Y',$stamp);
	}
	
	/*
	* calculate start of fertile period from LMP
	*/
	private function getFertileStartStamp($stamp) {
		//$factor = $this->cycle - $this->luteal;
		return strtotime('-5 days', $stamp);
	}
	
	/*
	* calculate end of fertile period from LMP
	*/
	private function getFertileEndStamp($stamp) {
		return strtotime('+1 days', $stamp);
	}
	
	
	
	// *****************************************************************
	
	// calculate due date from LMP
	private function dateFromLMP($stamp) {
		return strtotime('+280 days', $stamp);
	}
	
	// calculate ovulation/conception from LMP
	private function conceptionFromLMP($stamp) {
		return strtotime('+14 days', $stamp);
	}
	
	// calculate LMP from conception date
	private function lmpFromConception($stamp) {
		return strtotime('-14 days', $stamp);
	}
	
	/**
	* convert string to timestamp
	* @ string 
	* @return timestamp
	**/
	public function dateToTime($date) {
		return strtotime($date);
	}
	
}
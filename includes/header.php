<html lang="en"><head>
    <meta charset="utf-8">
    <title>Calculator Demo</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="stylesheet" href="css/bootstrap.css" media="screen">
    <link rel="stylesheet" href="/css/custom.css" media="screen">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
</head>
  <body cz-shortcut-listen="true">
  
    <div class="navbar navbar-default">
      <div class="container">
        <div class="navbar-header">
          <a href="" class="navbar-brand">Fertility Calculator</a>
          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>
        
        <div class="collapse navbar-collapse" id="navbar-main">
            <!-- Left Side Of Navbar -->
            <ul class="nav navbar-nav navbar-right">
                <!--<li><a href="/">Full Calculator</a></li>
                <li><a href="/lmp-to-due-date.php">LMP to Due Date</a></li>
                <li><a href="fertility-calculator.php">Fertility Calculator</a></li>
                <li><a href="conception-to-due-date.php">Conception to Due Date</a></li>-->
                <li><a href="http://jimbarrett.net" target="_blank">My Personal Site</a></li>
                <li><a href="https://bitbucket.org/barrettj1971/due-date-calculator"><i class="fa fa-bitbucket"></i> Get the code</a></li>
            </ul>
        </div>
        
      </div>
    </div>
    
    <div style="background-color:#BDD4DE;padding-top:1em;paddin-bottom:1em;">
      <div class="container">
      <div class="row">
        <div class="col-md-12">
            <p>This is a simple tool that can perform several calculations related to conception and fertility. Includes calculations for due date based on LMP or Conception date, likely Conception Date based on LMP, and next 6 fertile windows based on LMP.</p>
            <p>Feel free to use the tool as-is or <a href="https://bitbucket.org/barrettj1971/due-date-calculator" target="_blank">get the code</a> to use it in your own site or app. <a href="http://jimbarrett.net#contact" target="_blank">Contact me</a> if you would like to discuss ways that I can help you integrate a customized version of this calculator into your site. </p>
        </div>
      </div>
      </div>
    </div>
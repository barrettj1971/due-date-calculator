$(document).ready(function() {
	$('.calculateBy').on('change', function() {
		var v = $(this).val();
		if(v == 1) {
			$('#lmpDate').removeClass('hidden');
			$('#conceptionDate').addClass('hidden');
		} else {
			$('#lmpDate').addClass('hidden');
			$('#conceptionDate').removeClass('hidden');
		}
	});
});